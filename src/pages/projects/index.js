import React from 'react';
import { Link, graphql } from 'gatsby';
import Layout from '../../components/Layout';
import * as styles from '../../styles/project.module.css'
import Img from 'gatsby-image'

export default function Projects({ data }) {
  const projects = data.projects.nodes
  const contact = data.contact.siteMetadata.contact

  return (
    <Layout>
      <section>
        <div className={styles.portfolio}>
          <h2>Project</h2>
          <h3>Project & desc</h3>
          <div className={styles.projects}>
            {projects.map(project => (
              <Link to={'/projects/' + project.frontmatter.slug} key={project.id}>
                <div>
                  <Img fluid={project.frontmatter.thumb.childImageSharp.fluid}/>
                  <h3>{project.frontmatter.title}</h3>
                  <p>{ project.frontmatter.stack }</p>
                </div>
              </Link> 
            ))}
          </div>
          <p>Like what you se? Email me at {contact} for a quote!</p>
        </div>
      </section>
    </Layout>
  );
}

// export page query

export const query = graphql`
  query ProjectPage {
    projects: allMarkdownRemark(sort: {fields: frontmatter___date, order: DESC}) {
      nodes {
        frontmatter {
          slug
          stack
          title
          thumb {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        id
      }
    }
    contact: site {
      siteMetadata {
        contact
      }
    }
  }
`

