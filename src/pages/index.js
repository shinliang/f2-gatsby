import React from "react";
import { graphql, Link, useStaticQuery } from 'gatsby';
import Layout from "../components/Layout";
import * as styles from "../styles/home.module.css";
import Img from 'gatsby-image'

export default function Home({ data }) {
  // console.log(data);
  const getUserNBanner = useStaticQuery(graphql `
    query getUserNBanner {
      getUser {
        users{
          id
          name
        }
      }
      file(relativePath: {eq: "banner.png"}) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);
  // console.log(getUserNBanner.getUser)
  return (
    <Layout>
      <section className={styles.header}>
        <div>
          <h2>Hatsu gatsby!!!!!</h2>
          <h3>this is ma first web build by using gatsby</h3>
          <p>ini Paragraph dan content nya aamaahaaaa</p>
          <div>
            {getUserNBanner.getUser.users.map((user, i) => {
              return (
                <div key={i}>
                  <p>ID: {user.id}</p>
                  <p>Name: {user.name}</p>
                </div>
              )
            })}
          </div>
          <Link className={styles.btn} to="/projects">Projects</Link>
        </div>
        <Img fluid={getUserNBanner.file.childImageSharp.fluid}/>
      </section>
    </Layout>
  )
}