import React from 'react';
import Navbar from './Navbar';
import '../styles/global.css'

export default function Layouts({ children }) {
  return (
      <div className="layout">
          <Navbar />
          <div className='content'>
              {/* content ny disini ntar */}
              { children }
          </div>
          <footer>
              <p>ini copyright</p>
          </footer>
      </div>
  )
}
